export function saveScenarioInStorage(scenario) {
  const key = `scenario:${scenario.id}`;
  localStorage.setItem(key, JSON.stringify(scenario));
}
